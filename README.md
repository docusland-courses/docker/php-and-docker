# PHP and Dockerfiles

## Premier exercice de la journée.

Vous avez au sein du [dépôt suivant](https://gitlab.com/docusland-courses/php/sample-laravel), un projet php réalisé via un framework Laravel. 

Ce projet requiert un environnement php précis, certaines bibliothèques précises. L'ensemble des instructions permettant de lancer le projet sont pécisées dans le fichier README du dépôt./

Lisez le fichier `composer.json` afin de savoir quelle image php doit être utilisée.

Votre objectif ici est de réaliser un fichier Dockerfile permettant de builder le projet laravel. 

Ce dernier devra : 
- partir d'une image php permettant d'héberger ce projet
- cloner le projet laravel
- compiler le projet laravel via composer
- il est attendu d'avoir une base de données sqlite intégrée dans le conteneur.

### Bonus
De base le projet explique comment lancer laravel pour un environnement de dev. 

Il existe certaines étapes à réaliser lors d'une [mise en production d'une application Laravel](https://laravel.com/docs/11.x/deployment). 

Adaptez le `Dockerfile`.


## Second exercice.

Finalement, votre chef de projet souhaite modifier sa requête initiale. 
Votre projet devra gérer la base de données via du mariadb et ne plus gérer la data via du sqlite. 

Il n'est plus attendu non plus de tout gérer via le `Dockerfile`.
Implémentez un fichier `docker-compose.yml`, permettant de lancer vos conteneur docker. 

Votre application Laravel, une fois utilisée commence à générer des données ainsi que des logs. 

Mettez en place les volumes nécessaires au niveau de votre projet pour : 
 - historiser les logs : dossier `storage/logs`
 - backuper la base de données : dossier `/var/lib/mysql`

Pour le moment, ces fichiers backups devront être stockés dans un dossier backup. Avec en nom de fichier :
 - YYYY-MM-DD.sample-laravel.log
 - YYYY-MM.DD.sample-laravel-database.sql